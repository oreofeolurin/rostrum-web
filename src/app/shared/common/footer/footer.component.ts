import {Component, OnInit, Input} from '@angular/core';
import {environment} from '../../../../environments/environment';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})

export class FooterComponent implements OnInit {

    public STATIC_DOMAIN = environment.STATIC_DOMAIN;

    public currentYear: number = new Date().getFullYear();

    constructor() {
    }

    public ngOnInit() {
        // set animation for this page
//        this.animation = new TopbarAnimation(this.el);
    }

}
