import {Type, InjectionToken} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Utils} from '../../../core/helpers/utils';

export interface ModalExistRef {
    tag: string;
    resolve: any;
    forceClose: boolean;
}

export class ModalRef {

    constructor(private observable: Observable<ModalExistRef>) {
    }

    /**
     * Gets an observable that is notified when the dialog is finished closing.
     *
     * */
    public onExit() {
        return this.observable;
    }
}

interface ModalInput {
    token?: InjectionToken<any>;
    value?: any;
}

export class ModalInputs {
    public array: Array<ModalInput> = [];


    constructor(modalInput: ModalInput) {
        this.add(modalInput);
    }

    public add(modalInput: ModalInput) {
        this.array.push(modalInput);
    }

}

export class Modal {
    public static TAG_INJECTION_TOKEN = new InjectionToken<string>('tag');
    public tag: string;
    public component: Type<any>;
    public inputs: Array<ModalInput>;

    constructor(component: Type<any>, inputs?: ModalInputs | Array<ModalInput>) {
        this.tag = Utils.generateRandomID(10).toUpperCase();
        this.component = component;

        if (inputs instanceof ModalInputs) {
            this.inputs = inputs.array;
        } else {
            this.inputs = inputs || [];
        }
    }

    public getTag(): string {
        return this.tag;
    }

}
