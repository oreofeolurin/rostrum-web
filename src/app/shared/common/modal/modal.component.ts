import {
    Component, OnInit, OnDestroy, ViewChild, ViewContainerRef, ComponentFactoryResolver,
    Inject, PLATFORM_ID, Injector
} from '@angular/core';
import {isPlatformServer} from '@angular/common';
import {Modal, ModalExistRef} from './modal';
import {DOM} from '../../../core/helpers/dom';
import {ModalService} from '../../../core/services/modal.service';
import {Subscription} from 'rxjs/Subscription';
import {PublishComponent} from '../../../features/post/publish/publish.component';

declare const TweenMax: any;

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss'],
    entryComponents: [PublishComponent]
})

export class ModalComponent implements OnInit, OnDestroy {

    public modal: Modal;

    private newModalSubscription: Subscription;

    private modalExitSubscription: Subscription;

    private currentComponent = null;

    @ViewChild('componentContainer', {read: ViewContainerRef})
    public componentContainer: ViewContainerRef;

    private dom: DOM;

    constructor(@Inject(PLATFORM_ID) private platformId: Object,
                @Inject(ComponentFactoryResolver) private resolver: ComponentFactoryResolver,
                @Inject(ModalService) private modalService: ModalService) {
    }

    public ngOnInit() {

        if (isPlatformServer(this.platformId)) {
            return;
        }

        this.newModalSubscription = this.modalService.newModalStream.subscribe(modal => this.load(modal));
        this.modalExitSubscription = this.modalService.modalExitStream.subscribe(modalExitRef => this.exitView(modalExitRef));

        // set the dom
        this.dom = new DOM(document);

        // Close modal when clicked out of modal
        this.dom.selectOne('#modal-content-layer').bind('click', (event: Event) => {
            if (event.srcElement.getAttribute('id') === 'modal-content-layer') {
                this.exitView();
            }
        });
    }

    public load(modal: Modal) {
        if (modal == null || isPlatformServer(this.platformId)) {
            return;
        }

        // set the modal
        this.modal = modal;

        // lets make sure tag is in input
        this.modal.inputs.push({token: Modal.TAG_INJECTION_TOKEN, value: this.modal.tag});

        // Inputs need to be in the following format to be resolved properly
        const inputProviders = modal.inputs.map((input) => {
            return {provide: input.token, useValue: input.value};
        });
        // resolvedInputs = ReflectiveInjector.resolve(inputProviders);
        // resolvedInputs = Injector.create(inputProviders);

        // We create an injector out of the data we want to pass down and this components injector
        const injector = Injector.create(inputProviders, this.componentContainer.parentInjector);
        // let injector = ReflectiveInjector.resolveAndCreate(resolvedInputs, this.componentContainer.parentInjector);

        // We create a factory out of the component we want to create
        const factory = this.resolver.resolveComponentFactory(modal.component);

        // We create the component using the factory and the injector
        const component = factory.create(injector);

        // We insert the component into the dom container
        this.componentContainer.insert(component.hostView);

        // Destroy the previously created component
        if (this.currentComponent) {
            this.currentComponent.destroy();
        }

        this.currentComponent = component;

        // reveal animation
        this.reveal();

    }


    public reveal() {
        TweenMax.set('body', {'overflow': 'hidden'});
        const dynamicView = this.dom.selectOne('#modal-view').element;
        TweenMax.to(dynamicView, .5, {backgroundColor: 'rgba(255, 255, 255, 0.87)', 'display': 'flex', 'opacity': 1});

    }

    public removeView() {
        TweenMax.set('body', {'overflow-y': 'auto'});
        const dynamicView = this.dom.selectOne('#modal-view').element;
        const baseView = this.dom.selectOne('#base-view').element;

        TweenMax.to(dynamicView, .5, {opacity: 0, display: 'none'});
        TweenMax.to(baseView, .5, {
            filter: 'blur(0)', onComplete: () => {
                this.currentComponent.destroy();
            }
        });

    }

    public exitView(modal?: ModalExistRef) {

        if (typeof modal === 'undefined') {
            return this.modalService.notifyModalExit(this.modal.getTag(), null, true);
        }

        this.removeView();
    }

    public ngOnDestroy() {
        // prevent memory leak by unsubscribing
        this.modalExitSubscription.unsubscribe();
        this.newModalSubscription.unsubscribe();
    }

}
