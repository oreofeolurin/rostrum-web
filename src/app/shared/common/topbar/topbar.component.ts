import {Component, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {AuthService} from '../../../core/services/auth.service';
import {Router} from '@angular/router';
import {UserService} from '../../../store/user/user.service';
import {Observable} from 'rxjs/Observable';
import {User} from '../../../store/user/user.interface';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnDestroy {
    public STATIC_DOMAIN = environment.STATIC_DOMAIN;
    public user$: Subscription;

    @Input()
    public theme = 'light';

    @Output()
    public toggleSideNav = new EventEmitter<boolean>();
    public user: User;

    constructor(public router: Router,
                public authService: AuthService,
                private userService: UserService) {
        this.user$ = userService.getUser().subscribe(v => this.user = v);
    }

    public toggleMenu() {
        this.toggleSideNav.emit(true);
    }

    public onLogout() {
        this.userService.removeData();
        this.authService.logout();
        this.router.navigateByUrl('/');
    }

    public ngOnDestroy() {
        this.user$.unsubscribe();
}

}
