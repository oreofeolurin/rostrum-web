import {Component, Input, Inject, Output, EventEmitter, PLATFORM_ID, ElementRef} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {Router} from '@angular/router';
import {ModalService} from '../../../core/services/modal.service';
import {AuthService} from '../../../core/services/auth.service';
import {environment} from '../../../../environments/environment';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],
    animations: [
        trigger('activeState', [
            state('inactive', style({
                transform: 'scale(0.9)',
                opacity: 0.6
            })),
            state('active',   style({
                transform: 'scale(1)',
                opacity: 1
            })),
            transition('inactive => active', animate('200ms ease-in')),
            transition('active => inactive', animate('200ms ease-out'))
        ])]
})
export class SidebarComponent {

    public STATIC_DOMAIN = environment.STATIC_DOMAIN;

    @Output()
    public toggleSideNav = new EventEmitter<boolean>();

    public activeState = 'inactive';

    @Input()
    public set opened(o: boolean) { this.activeState  = o ? 'active' : 'inactive'; }

    constructor(@Inject(Router) private router: Router,
                @Inject(PLATFORM_ID) private platformId: Object,
                @Inject(ElementRef) private el: ElementRef,
                @Inject(ModalService) private modalService: ModalService,
                @Inject(AuthService) public authService: AuthService) {
    }

    public toggleMenu(isOpen?: boolean) {
        this.toggleSideNav.emit(isOpen);
    }

    public onLogin() {
    }


    public onLogout() {
        this.toggleMenu();
    }

    public onNavigate(redirectUrl: string) {
        this.toggleMenu();
        this.router.navigate([redirectUrl]);
    }

}

