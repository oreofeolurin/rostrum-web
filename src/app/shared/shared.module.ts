import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TopbarComponent} from './common/topbar/topbar.component';
import {SidebarComponent} from './common/sidebar/sidebar.component';
import {FooterComponent} from './common/footer/footer.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ModalComponent} from './common/modal/modal.component';
import { SmartDatePipe } from './pipes/smart-date.pipe';
import {MaterialModule} from './material/material.module';
import {RosPluralPipe} from './pipes/ros-plural.pipe';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        MaterialModule
    ],
    declarations: [
        ModalComponent,
        TopbarComponent,
        SidebarComponent,
        FooterComponent,

        RosPluralPipe,
        SmartDatePipe
    ],
    exports: [
        CommonModule,
        ReactiveFormsModule,
        MaterialModule,

        ModalComponent,
        TopbarComponent,
        SidebarComponent,
        FooterComponent,

        RosPluralPipe,
        SmartDatePipe,
    ],
})
export class SharedModule {}
