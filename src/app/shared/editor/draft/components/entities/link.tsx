import * as React from 'react';

import { Entity } from '../../utils/constants';

export const findLinkEntities = (contentBlock, callback, contentState) => {
    contentBlock.findEntityRanges(
        (character) => {
            const entityKey = character.getEntity();
            return (
                entityKey !== null &&
                contentState.getEntity(entityKey).getType() === Entity.LINK
            );
        },
        callback
    );
};

const Link = (props) => {
    const { contentState, entityKey } = props;
    const { url } = contentState.getEntity(entityKey).getData();
    return (
        <a className='ros-link'
            href={url}
            rel='noopener noreferrer'
            target='_blank'
            aria-label={url}
        >{props.children}</a>
    );
};

export default Link;
