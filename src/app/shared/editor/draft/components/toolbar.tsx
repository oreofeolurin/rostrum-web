import * as React from 'react';
import * as ReactDOM from 'react-dom';

import BlockToolbar from './block-toolbar';
import InlineToolbar from './inline-toolbar';

import {getSelection, getSelectionRect} from '../utils';
import {getCurrentBlock} from '../model';
import {Entity, HYPERLINK} from '../utils/constants';
import {BLOCK_BUTTONS, Button, INLINE_BUTTONS} from './buttons';
import {EditorState} from 'draft-js';

interface ToolbarProps {
    editorEnabled: boolean;
    editorState: EditorState;
    editorNode: HTMLElement;
    setLink: (url) => void;
    focus: () => void;
    toggleInlineStyle: (e) => void;
    toggleBlockType: (e) => void;
    blockButtons?: Array<Button>;
    inlineButtons?: Array<Button>;
}

interface ToolbarState {
    showURLInput: boolean;
    urlInputValue: string;
}

export class Toolbar extends React.Component<ToolbarProps, ToolbarState> {

    static defaultProps = {
        blockButtons: BLOCK_BUTTONS,
        inlineButtons: INLINE_BUTTONS,
    };

    private urlinput: any;

    constructor(props) {
        super(props);
        this.state = {
            showURLInput: false,
            urlInputValue: '',
        };

        this.onKeyDown = this.onKeyDown.bind(this);
        this.onChange = this.onChange.bind(this);
        this.handleLinkInput = this.handleLinkInput.bind(this);
        this.hideLinkInput = this.hideLinkInput.bind(this);
    }

    componentWillReceiveProps(newProps) {
        const {editorState} = newProps;
        if (!newProps.editorEnabled) {
            return;
        }
        const selectionState = editorState.getSelection();
        if (selectionState.isCollapsed()) {
            if (this.state.showURLInput) {
                this.setState({
                    showURLInput: false,
                    urlInputValue: '',
                });
            }
            return;
        }
    }

    // shouldComponentUpdate(newProps, newState) {
    //   console.log(newState, this.state);
    //   if (newState.showURLInput !== this.state.showURLInput && newState.urlInputValue !== this.state.urlInputValue) {
    //     return true;
    //   }
    //   return false;
    // }

    componentDidUpdate() {
        if (!this.props.editorEnabled || this.state.showURLInput) {
            return;
        }
        const selectionState = this.props.editorState.getSelection();
        if (selectionState.isCollapsed()) {
            return;
        }

        const nativeSelection = getSelection(window);
        if (!nativeSelection.rangeCount) {
            return;
        }
        const selectionBoundary = getSelectionRect(nativeSelection);

        const toolbarNode = ReactDOM.findDOMNode(this) as HTMLElement;
        const toolbarBoundary = toolbarNode.getBoundingClientRect();


        const parent = ReactDOM.findDOMNode(this.props.editorNode);
        const parentBoundary = parent.getBoundingClientRect();

        /*
        * Main logic for setting the toolbar position.
        */
        toolbarNode.style.top =
            `${(selectionBoundary.top - parentBoundary.top - toolbarBoundary.height - 5)}px`;

        toolbarNode.style.width = `${toolbarBoundary.width}px`;
        const widthDiff = selectionBoundary.width - toolbarBoundary.width;
        if (widthDiff >= 0) {
            toolbarNode.style.left = `${widthDiff / 2}px`;
        } else {
            const left = (selectionBoundary.left - parentBoundary.left);
            toolbarNode.style.left = `${left + (widthDiff / 2)}px`;
            // toolbarNode.style.width = toolbarBoundary.width + 'px';
            // if (left + toolbarBoundary.width > parentBoundary.width) {
            // toolbarNode.style.right = '0px';
            // toolbarNode.style.left = '';
            // toolbarNode.style.width = toolbarBoundary.width + 'px';
            // }
            // else {
            //   toolbarNode.style.left = (left + widthDiff / 2) + 'px';
            //   toolbarNode.style.right = '';
            // }
        }
    }

    onKeyDown(e) {
        if (e.which === 13) {
            e.preventDefault();
            e.stopPropagation();
            this.props.setLink(this.state.urlInputValue);
            this.hideLinkInput();
        } else if (e.which === 27) {
            this.hideLinkInput();
        }
    }

    onChange(e) {
        this.setState({
            urlInputValue: e.target.value,
        });
    }

    handleLinkInput(e, direct = false) {
        if (direct !== true) {
            e.preventDefault();
            e.stopPropagation();
        }
        const {editorState} = this.props;
        const selection = editorState.getSelection();
        if (selection.isCollapsed()) {
            this.props.focus();
            return;
        }
        const currentBlock = getCurrentBlock(editorState);
        let selectedEntity = '';
        let linkFound = false;
        currentBlock.findEntityRanges((character) => {
            const entityKey = character.getEntity();
            selectedEntity = entityKey;
            return entityKey !== null && editorState.getCurrentContent().getEntity(entityKey).getType() === Entity.LINK;
        }, (start, end) => {
            let selStart = selection.getAnchorOffset();
            let selEnd = selection.getFocusOffset();
            if (selection.getIsBackward()) {
                selStart = selection.getFocusOffset();
                selEnd = selection.getAnchorOffset();
            }
            if (start === selStart && end === selEnd) {
                linkFound = true;
                const {url} = editorState.getCurrentContent().getEntity(selectedEntity).getData();
                this.setState({
                    showURLInput: true,
                    urlInputValue: url,
                }, () => {
                    setTimeout(() => {
                        this.urlinput.focus();
                        this.urlinput.select();
                    }, 0);
                });
            }
        });
        if (!linkFound) {
            this.setState({
                showURLInput: true,
            }, () => {
                setTimeout(() => {
                    this.urlinput.focus();
                }, 0);
            });
        }
    }

    hideLinkInput(e = null) {
        if (e !== null) {
            e.preventDefault();
            e.stopPropagation();
        }
        this.setState({
                showURLInput: false,
                urlInputValue: '',
            }, this.props.focus
        );
    }

    render() {
        const {editorState, editorEnabled, inlineButtons} = this.props;
        const {showURLInput, urlInputValue} = this.state;
        let isOpen = true;
        if (!editorEnabled || editorState.getSelection().isCollapsed()) {
            isOpen = false;
        }
        if (showURLInput) {
            let className = `ros-editor-toolbar${(isOpen ? ' ros-editor-toolbar--isopen' : '')}`;
            className += ' ros-editor-toolbar--linkinput';
            return (
                <div className={className}>
                    <div className='ros-PostEditor-controls ros-PostEditor-show-link-input' style={{display: 'block'}}>
                        <span className='ros-url-input-close' onClick={this.hideLinkInput}>&times;</span>
                        <input ref={node => {
                            this.urlinput = node;
                        }} type='text' className='ros-url-input'
                               onKeyDown={this.onKeyDown}
                               onChange={this.onChange}
                               placeholder='Press ENTER or ESC'
                               value={urlInputValue}
                        />
                    </div>
                </div>
            );
        }

        let hasHyperLink = false;
        let hyperlinkLabel = '#';
        let hyperlinkDescription = 'Add a link';
        for (let cnt = 0; cnt < inlineButtons.length; cnt++) {
            if (inlineButtons[cnt].style === HYPERLINK) {
                hasHyperLink = true;
                if (inlineButtons[cnt].label) {
                    hyperlinkLabel = inlineButtons[cnt].label;
                }
                if (inlineButtons[cnt].description) {
                    hyperlinkDescription = inlineButtons[cnt].description;
                }
                break;
            }
        }

        let renderBlockButtons = null;
        let renderInlineButtons = null;
        if (this.props.blockButtons.length > 0) {
            renderBlockButtons = <BlockToolbar editorState={editorState}
                                               onToggle={this.props.toggleBlockType} buttons={this.props.blockButtons}/>;
        }

        if (this.props.inlineButtons.length > 0) {
            renderInlineButtons = <InlineToolbar
                editorState={editorState}
                onToggle={this.props.toggleInlineStyle}
                buttons={this.props.inlineButtons}
            />;
        }

        return (
            <div className={`ros-editor-toolbar${(isOpen ? ' ros-editor-toolbar--isopen' : '')}`}>
                {renderBlockButtons}
                {renderInlineButtons}
                {hasHyperLink && (
                    <div className='ros-PostEditor-controls'>
                        <span className='ros-PostEditor-styleButton ros-PostEditor-linkButton hint--top hint--rounded'
                              onClick={this.handleLinkInput} aria-label={hyperlinkDescription}>
                {hyperlinkLabel}
                </span>
                    </div>
                )}
            </div>
        );
    }
}




