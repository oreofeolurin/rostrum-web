import * as React from 'react';

import {StyleButton} from './style-button';


const InlineToolbar = (props) => {
    if (props.buttons.length < 1) {
        return null;
    }
    const currentStyle = props.editorState.getCurrentInlineStyle();
    return (
        <div className='ros-PostEditor-controls ros-PostEditor-controls-inline'>
            {props.buttons.map(type => {
                const iconLabel: any = {};
                iconLabel.label = type.label;
                return (
                    <StyleButton
                        {...iconLabel}
                        key={type.style}
                        active={currentStyle.has(type.style)}
                        onToggle={props.onToggle}
                        style={type.style}
                        description={type.description}
                    />
                );
            })}
        </div>
    );
};


export default InlineToolbar;
