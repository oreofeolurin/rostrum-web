import * as React from 'react';
import {HYPERLINK} from '../utils/constants';

interface StyleButtonProps {
    style: string;
    onToggle: (e) => void;
    active: boolean;
    description: string;
    icon: string;
    label: string;
}


export class StyleButton extends React.Component<StyleButtonProps> {

    onToggle:  StyleButtonProps['onToggle'];

    constructor(props) {
        super(props);
        this.onToggle = (e) => {
            e.preventDefault();
            this.props.onToggle(this.props.style);
        };
    }

    render() {
        if (this.props.style === HYPERLINK) {
            return null;
        }
        let className = 'ros-PostEditor-styleButton';
        if (this.props.active) {
            className += ' ros-PostEditor-activeButton';
        }
        className += ` ros-PostEditor-styleButton-${this.props.style.toLowerCase()}`;
        return (
            <span
                className={`${className} hint--top`}
                onMouseDown={this.onToggle}
                aria-label={this.props.description}
            >
        {this.props.icon ? <i className={`fa fa-${this.props.icon}`}/> : this.props.label}
      </span>
        );
    }
}
