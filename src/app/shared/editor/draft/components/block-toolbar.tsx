import * as React from 'react';
import { RichUtils } from 'draft-js';
import {StyleButton} from './style-button';

const BlockToolbar = (props) => {
    if (props.buttons.length < 1) {
        return null;
    }
    const { editorState } = props;
    const blockType = RichUtils.getCurrentBlockType(editorState);
    return <div className='ros-PostEditor-controls ros-PostEditor-controls-block'>
        {props.buttons.map((type) => {
            const iconLabel: any = {};
            iconLabel.label = type.label;
            return (
                <StyleButton
                    {...iconLabel}
                    key={type.style}
                    active={type.style === blockType}
                    onToggle={props.onToggle}
                    style={type.style}
                    description={type.description}
                />
            );
        })}
    </div>;
};


export default BlockToolbar;

