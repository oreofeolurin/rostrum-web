import { Inline } from './constants';

/*
Custom style map for custom entities like Hihglight.
*/
export const customStyleMap = {
    [Inline.HIGHLIGHT]: {
        backgroundColor: 'yellow',
    },
    [Inline.CODE]: {
        fontFamily: 'SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace',
        margin: '4px 3px',
        fontSize: '0.9em',
        padding: '4px 6px',
        backgroundColor: '#f5f4f4'
    }
};




/*

// Custom overrides for "code" style.
const styleMap = {
    CODE: {
        backgroundColor: 'rgba(0, 0, 0, 0.05)',
        fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
        fontSize: 16,
        padding: 2
    }
};
*/
