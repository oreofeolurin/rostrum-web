export class Log {
    static trigger (eventName: string) {
        console.log(`${eventName} event triggered`);
    }

    static valueOf(variable: string, value) {
        console.log(`${variable}  =>  ${value}`);
    }
}
