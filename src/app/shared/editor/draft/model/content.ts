import {
    EditorState,
    convertFromRaw,
    CompositeDecorator,
    ContentState, convertFromHTML,
} from 'draft-js';

declare var require: any;

// This package lacks for type definitions
const stateToHTML = require('draft-js-export-html').stateToHTML;

import Link, {findLinkEntities} from '../components/entities/link';
import {renderToHTML} from '../utils/export';
import {convertContentFromHTML} from '../utils/importer';


const defaultDecorators = new CompositeDecorator([
    {
        strategy: findLinkEntities,
        component: Link,
    },
]);


export function createEditorState(content = null, decorators = defaultDecorators) {

    if (content) {
        let contentState = null;
        if (content.raw) {
            console.log('from raw');
            contentState = convertFromRaw(JSON.parse(content.raw));
        } else if (content.html) {
             contentState = convertContentFromHTML(content.html);
        }

        return contentState !== null && EditorState.createWithContent(contentState, decorators);
    }

    return EditorState.createEmpty(decorators);

}

function createFromHTML(html: string) {

    const blocksFromHTML = convertFromHTML(html);
    let state: ContentState;

    try {
        const contentBlocks = blocksFromHTML.contentBlocks;
        const entityMap = blocksFromHTML.entityMap;
        state = ContentState.createFromBlockArray(contentBlocks, entityMap);
    } catch (e) {
        return null;
    }

    return state;
}


export function renderContentToHTML(state: EditorState) {

    return renderToHTML(state.getCurrentContent());

}



export function renderHTMLToText(html: string) {

    const tmp = document.createElement('DIV');
    tmp.innerHTML = html;
    return tmp.textContent.trim() || tmp.innerText.trim() || '';
}

//
// export function renderContentToHTML(state: EditorState) {
//
//     return renderToHTML(state.getCurrentContent());
//
// }
