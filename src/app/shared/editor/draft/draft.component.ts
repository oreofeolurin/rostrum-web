import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as React from 'react';
import {EditorState, ContentState, convertFromHTML, convertToRaw} from 'draft-js';
import {createEditorState, renderContentToHTML, renderHTMLToText} from './model/content';


export class DraftBase {
    @Input() editorClass: React.Component;

    // TODO: Type props?
    @Input() editorProps: any = {
        editorState: EditorState.createEmpty(),
    };

    @Input()
    set key(key: string) {
        this.editorProps = Object.assign({}, this.editorProps, {key: key});
    }
}


export class DraftHtmlBase extends DraftBase implements OnInit {
    private state: EditorState;

    @Input()
    set content(content) {
        this.state = createEditorState(content);
        this.editorProps = Object.assign({}, this.editorProps, {editorState: this.state});
    }

    @Output() save = new EventEmitter<object>();

    ngOnInit() {
        this.editorProps = Object.assign({}, this.editorProps, {
            onChange: editorState => this.state = editorState,
            onSave: () => {
                const content = this.getEditorContent();
                this.save.emit(content);
            }
        });
    }

    public getEditorContent() {
        if (this.state) {
            const html = renderContentToHTML(this.state);
            const text = renderHTMLToText(html);
            const raw = JSON.stringify(convertToRaw(this.state.getCurrentContent()));

            if (text.length > 0) {
                return {html, raw};
            }
        }

        return null;


    }
}

@Component({
    selector: 'app-draft',
    template: `
        <app-react-wrapper
            [reactClass]="editorClass"
            [reactProps]="editorProps">
        </app-react-wrapper>
    `,
    styleUrls: ['./draft.component.scss'],
})
export class DraftComponent extends DraftBase {
}

