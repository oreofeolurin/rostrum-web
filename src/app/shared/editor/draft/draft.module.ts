import { NgModule } from '@angular/core';
import { ReactWrapperComponent } from './react-wrapper.component';
import { DraftComponent } from './draft.component';

@NgModule({
  declarations: [ReactWrapperComponent, DraftComponent],
  exports: [ReactWrapperComponent, DraftComponent],
})
export class DraftModule { }
