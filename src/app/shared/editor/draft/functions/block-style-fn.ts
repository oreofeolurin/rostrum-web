import {Block} from '../utils/constants';

const BASE_BLOCK_CLASS = 'ros-block';

/*
Get custom class names for each of the different block types supported.
*/
export function blockStyleFn(block) {
    switch (block.getType()) {
        case Block.H1:
            return `${BASE_BLOCK_CLASS} ${BASE_BLOCK_CLASS}-header-one`;
         case Block.H2:
            return `${BASE_BLOCK_CLASS} ${BASE_BLOCK_CLASS}-header-two`;
            case Block.H3:
            return `${BASE_BLOCK_CLASS} ${BASE_BLOCK_CLASS}-header-three`;
        case Block.BLOCKQUOTE:
            return `${BASE_BLOCK_CLASS} ${BASE_BLOCK_CLASS}-quote ros-PostEditor-blockquote`;
        case Block.UNSTYLED:
            return `${BASE_BLOCK_CLASS} ${BASE_BLOCK_CLASS}-paragraph`;
        case Block.ATOMIC:
            return `${BASE_BLOCK_CLASS} ${BASE_BLOCK_CLASS}-atomic`;
        case Block.CAPTION:
            return `${BASE_BLOCK_CLASS} ${BASE_BLOCK_CLASS}-caption`;
        case Block.TODO: {
            const data = block.getData();
            const checkedClass = data.get('checked') === true ?
                `${BASE_BLOCK_CLASS}-todo-checked` : `${BASE_BLOCK_CLASS}-todo-unchecked`;
            let finalClass = `${BASE_BLOCK_CLASS} ${BASE_BLOCK_CLASS}-paragraph `;
            finalClass += `${BASE_BLOCK_CLASS}-todo ${checkedClass}`;
            return finalClass;
        }
        case Block.IMAGE:
            return `${BASE_BLOCK_CLASS} ${BASE_BLOCK_CLASS}-image`;
        case Block.BLOCKQUOTE_CAPTION: {
            const cls = `${BASE_BLOCK_CLASS} ${BASE_BLOCK_CLASS}-quote`;
            return `${cls} ros-PostEditor-blockquote ${BASE_BLOCK_CLASS}-quote-caption`;
        }
        default: return BASE_BLOCK_CLASS;
    }
}
