import {EditorState} from 'draft-js';
import {Block} from '../../draft/utils/constants';
import {beforeInput, StringToTypeMap} from '../../draft/utils/before-input';
import {keyBindingFn} from '../../draft/utils/key-binding';
import BlockRenderMap from '../../draft/utils/block-render-map';

export interface PostEditorState {
    editorState: EditorState;
}

export interface PostEditorProps {/*
    blockButtons: any;*/
    placeholder: string;
    spellCheck: boolean;
    editorEnabled: boolean;
    blockTypes?: { string: string[] };
    inlineStyles?: { string: string[] };
    disableToolbar: boolean;
    showLinkEditToolbar: boolean;
    processURL: (e) => string;
    continuousBlocks: string [];
    beforeInput: (a, b, c, d) => void;
    stringToTypeMap: any;
    blockRenderMap: any;
    handleReturn: () => string | boolean;
    handleKeyCommand: (c) => string;
    keyBindingFn: any;
}


export const POST_EDITOR_DEFAULT_PROPS = {
    placeholder: 'Write something awesome...',
    spellCheck: true,
    editorEnabled: true,
    disableToolbar: false,
    processURL: undefined,
    showLinkEditToolbar: true,
    continuousBlocks: [
        Block.UNSTYLED,
        Block.BLOCKQUOTE,
        Block.OL,
        Block.UL,
        Block.CODE,
        Block.TODO
    ],
    handleReturn: undefined,
    handleKeyCommand: undefined,
    beforeInput: beforeInput,
    stringToTypeMap: StringToTypeMap,
    blockRenderMap: BlockRenderMap,
    keyBindingFn: keyBindingFn
};
