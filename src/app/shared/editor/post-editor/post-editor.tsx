import * as React from 'react';
import {SelectionState, Editor, EditorState, RichUtils, Modifier, KeyBindingUtil} from 'draft-js';
import * as isSoftNewlineEvent from 'draft-js/lib/isSoftNewlineEvent';


import {blockStyleFn} from '../draft/functions/block-style-fn';
import {POST_EDITOR_DEFAULT_PROPS, PostEditorProps, PostEditorState} from './utils/config';
import {Log} from '../draft/utils/logger';
import {Toolbar} from '../draft/components/toolbar';
import {Block, Entity as E, HANDLED, KEY_COMMANDS, NOT_HANDLED} from '../draft/utils/constants';
import {customStyleMap} from '../draft/utils/custorm-style-map';
import {addNewBlockAt, CursorLink, getCurrentBlock, isCursorBetweenLink, resetBlockWithType} from '../draft/model';
import {LinkEdit} from '../draft/components/link-edit';

export class PostEditor extends React.Component<PostEditorProps, PostEditorState> {

    private config: PostEditorProps;
    private editorNode;
    private toolbar: any;

    private handleKeyCommand: any;
    private handleReturn: any;
    private handleBeforeInput: any;
    private onChange: (editorState, cb?) => void;
    private onSave: () => void;

    focus = () => this.editorNode.focus();


    constructor(props) {
        super(props);

        // lets set configurations for this editors
        this.config = POST_EDITOR_DEFAULT_PROPS;
        this.state = {editorState: props.editorState};

        // Set the state an notify of new change
        this.onChange = (editorState, cb?) => { this.setState({editorState}, cb); props.onChange(editorState); };

        // Sends request for an immediate save
        this.onSave = () => props.onSave && props.onSave();

        this.handleKeyCommand = this._handleKeyCommand.bind(this) as any;
        this.handleReturn = this._handleReturn.bind(this);
        this.handleBeforeInput = this._handleBeforeInput.bind(this);
    }


    /**
     * Implemented to provide nesting of upto 2 levels in ULs or OLs.
     */
    onTab = (e) => {
        const {editorState} = this.state;
        const maxDepth = 4;
        const newEditorState = RichUtils.onTab(e, editorState, maxDepth);
        if (newEditorState !== editorState) {
            this.onChange(newEditorState);
        }
    }

    /*
    Adds a hyperlink on the selected text with some basic checks.
    */
    setLink = (url) => {
        let {editorState} = this.state;
        const selection = editorState.getSelection();
        const content = editorState.getCurrentContent();
        let entityKey = null;
        let newUrl = url;
        if (this.props.processURL) {
            newUrl = this.props.processURL(url);
        } else if (url.indexOf('http') === -1) {
            if (url.indexOf('@') >= 0) {
                newUrl = `mailto:${newUrl}`;
            } else {
                newUrl = `http://${newUrl}`;
            }
        }
        if (newUrl !== '') {
            const contentWithEntity = content.createEntity(E.LINK, 'MUTABLE', {url: newUrl});
            editorState = EditorState.push(editorState, contentWithEntity, 'create-entity' as any);
            entityKey = contentWithEntity.getLastCreatedEntityKey();
        }

        this.onChange(RichUtils.toggleLink(editorState, selection, entityKey), this.focus);

    }

    /*
  Handles custom commands based on various key combinations. First checks
  for some built-in commands. If found, that command's function is apllied and returns.
  If not found, it checks whether parent component handles that command or not.
  Some of the internal commands are:

  - showlinkinput -> Opens up the link input tooltip if some text is selected.
  - add-new-block -> Adds a new block at the current cursor position.
  - changetype:block-type -> If the command starts with `changetype:` and
    then succeeded by the block type, the current block will be converted to that particular type.
  - toggleinline:inline-type -> If the command starts with `toggleinline:` and
    then succeeded by the inline type, the current selection's inline type will be
    togglled.
  */
    _handleKeyCommand(command) {
        // console.log(command);
        const {editorState} = this.state;
        if (this.config.handleKeyCommand) {
            const behaviour = this.config.handleKeyCommand(command);
            if (behaviour === HANDLED || behaviour) {
                return HANDLED;
            }
        }

        if (command ===  KEY_COMMANDS.editorSave()) {
            this.onSave();
            return HANDLED;
        }

        if (command === KEY_COMMANDS.showLinkInput()) {
            if (!this.props.disableToolbar && this.toolbar) {
                // For some reason, scroll is jumping sometimes for the below code.
                // Debug and fix it later.
                const isCursorLink = isCursorBetweenLink(editorState);
                if (isCursorLink) {
                    this.editLinkAfterSelection(isCursorLink.blockKey, isCursorLink.entityKey);
                    return HANDLED;
                }
                this.toolbar.handleLinkInput(null, true);
                return HANDLED;
            }
            return NOT_HANDLED;
        } else if (command === KEY_COMMANDS.unlink()) {
            const isCursorLink = isCursorBetweenLink(editorState);
            if (isCursorLink) {
                this.removeLink(isCursorLink.blockKey, isCursorLink.entityKey);
                return HANDLED;
            }
        }
        /* else if (command === KEY_COMMANDS.addNewBlock()) {
          const { editorState } = this.props;
          this.onChange(addNewBlock(editorState, Block.BLOCKQUOTE));
          return HANDLED;
        } */
        const block = getCurrentBlock(editorState);
        const currentBlockType = block.getType();
        // if (command === KEY_COMMANDS.deleteBlock()) {
        //   if (currentBlockType.indexOf(Block.ATOMIC) === 0 && block.getText().length === 0) {
        //     this.onChange(resetBlockWithType(editorState, Block.UNSTYLED, { text: '' }));
        //     return HANDLED;
        //   }
        //   return NOT_HANDLED;
        // }
        if (command.indexOf(`${KEY_COMMANDS.changeType()}`) === 0) {
            let newBlockType = command.split(':')[1];
            // const currentBlockType = block.getType();
            if (currentBlockType === Block.ATOMIC) {
                return HANDLED;
            }
            if (currentBlockType === Block.BLOCKQUOTE && newBlockType === Block.CAPTION) {
                newBlockType = Block.BLOCKQUOTE_CAPTION;
            } else if (currentBlockType === Block.BLOCKQUOTE_CAPTION && newBlockType === Block.CAPTION) {
                newBlockType = Block.BLOCKQUOTE;
            }
            this.onChange(RichUtils.toggleBlockType(editorState, newBlockType));
            return HANDLED;
        } else if (command.indexOf(`${KEY_COMMANDS.toggleInline()}`) === 0) {
            const inline = command.split(':')[1];
            this.toggleInlineStyle(inline);
            return HANDLED;
        }
        const newState = RichUtils.handleKeyCommand(editorState, command);
        if (newState) {
            this.onChange(newState);
            return HANDLED;
        }
        return NOT_HANDLED;
    }

    /*
    This function is responsible for emitting various commands based on various key combos.
    */
    _handleBeforeInput(str) {
        return this.config.beforeInput(
            this.state.editorState, str, this.onChange, this.config.stringToTypeMap);
    }

    /*
    By default, it handles return key for inserting soft breaks (BRs in HTML) and
    also instead of inserting a new empty block after current empty block, it first check
    whether the current block is of a type other than `unstyled`. If yes, current block is
    simply converted to an unstyled empty block. If RETURN is pressed on an unstyled block
    default behavior is executed.
    */
    _handleReturn(e) {
        if (this.config.handleReturn) {
            const behavior = this.config.handleReturn();
            if (behavior === HANDLED || behavior === true) {
                return HANDLED;
            }
        }
        const {editorState} = this.state;
        if (isSoftNewlineEvent(e)) {
            this.onChange(RichUtils.insertSoftNewline(editorState));
            return HANDLED;
        }
        if (!e.altKey && !e.metaKey && !e.ctrlKey) {
            const currentBlock = getCurrentBlock(editorState);
            const blockType = currentBlock.getType();

            if (blockType.indexOf(Block.ATOMIC) === 0) {
                this.onChange(addNewBlockAt(editorState, currentBlock.getKey()));
                return HANDLED;
            }

            if (currentBlock.getLength() === 0) {
                switch (blockType) {
                    case Block.UL:
                    case Block.OL:
                    case Block.BLOCKQUOTE:
                    case Block.BLOCKQUOTE_CAPTION:
                    case Block.CAPTION:
                    case Block.TODO:
                    case Block.H2:
                    case Block.H3:
                    case Block.H1:
                        this.onChange(resetBlockWithType(editorState, Block.UNSTYLED));
                        return HANDLED;
                    default:
                        return NOT_HANDLED;
                }
            }

            const selection = editorState.getSelection();

            if (selection.isCollapsed() && currentBlock.getLength() === selection.getStartOffset()) {
                if (this.config.continuousBlocks.indexOf(blockType) < 0) {
                    this.onChange(addNewBlockAt(editorState, currentBlock.getKey()));
                    return HANDLED;
                }
                return NOT_HANDLED;
            }
            return NOT_HANDLED;
        }
        return NOT_HANDLED;
    }


    /**
     * Check to make sure the type is not 'atomic' if not pass it to draft to handle it
     * @param {string} blockType
     */
    toggleBlockType = (blockType: string) => {
        const type = RichUtils.getCurrentBlockType(this.state.editorState);
        if (type.indexOf(`${Block.ATOMIC}:`) === 0) {
            return;
        }

        this.onChange(RichUtils.toggleBlockType(this.state.editorState, blockType));
    }

    /**
     * Pass it to draft to handle it
     * @param {string} inlineStyle
     */
    toggleInlineStyle = (inlineStyle: string) => {
        this.onChange(
            RichUtils.toggleInlineStyle(this.state.editorState, inlineStyle)
        );
    }

    removeLink = (blockKey, entityKey) => {
        const {editorState} = this.state;
        const content = editorState.getCurrentContent();
        const block = content.getBlockForKey(blockKey);
        const oldSelection = editorState.getSelection();
        block.findEntityRanges((character) => {
            const eKey = character.getEntity();
            return eKey === entityKey;
        }, (start, end) => {
            const selection = new SelectionState({
                anchorKey: blockKey,
                focusKey: blockKey,
                anchorOffset: start,
                focusOffset: end,
            });
            const newEditorState = EditorState.forceSelection(RichUtils.toggleLink(editorState, selection, null), oldSelection);
            this.onChange(newEditorState, this.focus);
        });
    }


    editLinkAfterSelection = (blockKey, entityKey = null) => {
        if (entityKey === null) {
            return;
        }
        const {editorState} = this.state;
        const content = editorState.getCurrentContent();
        const block = content.getBlockForKey(blockKey);
        block.findEntityRanges((character) => {
            const eKey = character.getEntity();
            return eKey === entityKey;
        }, (start, end) => {
            const selection = new SelectionState({
                anchorKey: blockKey,
                focusKey: blockKey,
                anchorOffset: start,
                focusOffset: end,
            });
            const newEditorState = EditorState.forceSelection(editorState, selection);
            this.onChange(newEditorState);
            setTimeout(() => {
                if (this.toolbar) {
                    this.toolbar.handleLinkInput(null, true);
                }
            }, 100);
        });
    }

    /**
     * Handle pasting when cursor is in an image block. Paste the text as the
     * caption. Otherwise, let Draft do its thing.
     */
    handlePastedText = (text, html, es): any => {
        const {editorState} = this.state;
        const currentBlock = getCurrentBlock(editorState);
        if (currentBlock.getType() === Block.IMAGE) {
            const content = editorState.getCurrentContent();
            this.onChange(
                (EditorState as any).push(editorState, Modifier.insertText(content, editorState.getSelection(), text))
            );

            return HANDLED;
        }

        return NOT_HANDLED;
    }


    /**
     * Set custorm keybindings for post-editor
     * @param e
     * @return {any}
     */
    keyBinding = (e) => {
        if (KeyBindingUtil.hasCommandModifier(e)) {
            if (e.which === 83) {  /* Key S */
                return 'editor-save';
            }
        }
        return this.config.keyBindingFn(e);
    }

    render() {
        const {editorState} = this.state;
        const {editorEnabled, disableToolbar} = this.props;
        const {showLinkEditToolbar, blockRenderMap, placeholder, spellCheck} = this.config;

        const className = `ros-PostEditor-editor${!editorEnabled ? ' ros-PostEditor-readonly' : ''}`;
        let isCursorLink: boolean | CursorLink = false;
        if (editorEnabled && showLinkEditToolbar) {
            isCursorLink = isCursorBetweenLink(editorState);
        }

        return (
            <div className='ros-PostEditor-root'>
                <div className={className} onClick={this.focus}>

                    <Editor
                        ref={n => this.editorNode = n}
                        onChange={this.onChange}
                        onTab={this.onTab}
                        editorState={editorState}
                        blockRenderMap={blockRenderMap}
                        handleKeyCommand={this.handleKeyCommand}
                        handleReturn={this.handleReturn}
                        handleBeforeInput={this.handleBeforeInput}
                        handlePastedText={this.handlePastedText}
                        customStyleMap={customStyleMap}
                        blockStyleFn={blockStyleFn}
                        readOnly={!editorEnabled}
                        keyBindingFn={this.keyBinding}
                        placeholder={placeholder}
                        spellCheck={editorEnabled && spellCheck}/>
                </div>

                {!disableToolbar && (
                    <Toolbar
                        ref={c => this.toolbar = c}
                        editorNode={this.editorNode}
                        editorState={editorState}
                        toggleBlockType={this.toggleBlockType}
                        toggleInlineStyle={this.toggleInlineStyle}
                        editorEnabled={editorEnabled}
                        setLink={this.setLink}
                        focus={this.focus}
                    />
                )}

                {isCursorLink && (
                    <LinkEdit
                        {...isCursorLink}
                        editorState={editorState}
                        removeLink={this.removeLink}
                        editLink={this.editLinkAfterSelection}
                    />)}

            </div>);

    }
}
