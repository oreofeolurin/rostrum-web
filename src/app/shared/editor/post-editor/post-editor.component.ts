import {Component, Input} from '@angular/core';
import {DraftHtmlBase} from '../draft/draft.component';
import {PostEditor} from './post-editor';

@Component({
  selector: 'app-post-editor',
  template: '<app-draft [editorClass]="editorClass" [editorProps]="editorProps"></app-draft>',
  styleUrls: ['../draft/draft.component.scss']
})

export class PostEditorComponent extends DraftHtmlBase {
    editorClass = PostEditor as any;

    @Input()
    set placeholder(placeholder: string) {
        this.editorProps = Object.assign({}, this.editorProps, { placeholder});
    }


    @Input()
    set editorEnabled(editorEnabled: boolean) {
        this.editorProps = Object.assign({}, this.editorProps, { editorEnabled});
    }

    @Input()
    set spellCheck(spellCheck: boolean) {
        this.editorProps = Object.assign({}, this.editorProps, { spellCheck});
    }

}
