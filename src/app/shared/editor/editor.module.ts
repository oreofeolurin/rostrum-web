import { NgModule } from '@angular/core';
import {DraftModule} from './draft/draft.module';
import { PostEditorComponent } from './post-editor/post-editor.component';

@NgModule({
  imports: [
      DraftModule,
  ],
  declarations: [PostEditorComponent],
  exports: [PostEditorComponent]
})
export class EditorModule { }

