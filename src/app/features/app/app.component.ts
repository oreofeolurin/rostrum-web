import { Component } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {UIService} from '../../store/ui/ui.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})

export class AppComponent {
    public isLoading$: Observable<boolean>;

    constructor(uiService: UIService) {
        this.isLoading$ = uiService.getIsLoading();
    }
}
