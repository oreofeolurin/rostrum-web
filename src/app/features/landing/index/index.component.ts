import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Category, Post} from '../../../store';
import {PostService} from '../../../store/post/post.service';
import {CategoryActions} from '../../../store/category/category-actions';
import {CategoryService} from '../../../store/category/category.service';
import {UIActions} from '../../../store/ui/ui-actions';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

    public categories$: Observable<Array<Category>>;
    private posts: Array<Post> = [];

    constructor(private postService: PostService,
                categoryService: CategoryService,
                private uiActions: UIActions) {

        // this.posts$ = postService.getLatestPosts();
        this.categories$ = categoryService.getCategories();

        // Lets fetch all need resources on the home
        this.fetchHomeResources();
    }

    ngOnInit(): void {
    }


    private fetchHomeResources() {

        this.uiActions.load();
        this.postService.fetchLatestPosts().subscribe(posts => {
            this.uiActions.loadDone();
            this.posts = this.posts.concat(posts);
        });

    }


}
