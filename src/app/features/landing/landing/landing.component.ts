import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSidenav} from '@angular/material';
import { Location } from '@angular/common';
import {NavigationEnd, Router} from '@angular/router';

@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss']
})

export class LandingComponent implements OnInit {
    public sideNavOpened = false;
    public theme = 'light';

    @ViewChild('sideNav') sideNav: MatSidenav;

    constructor(private router: Router, /*location: Location*/) {
        // this.setTheme(location.path());
    }

    public ngOnInit() {

      //  this.router.events.subscribe(e => e instanceof NavigationEnd ? this.setTheme(e.url) : null);
        this.sideNav.openedChange.subscribe(o => this.sideNavOpened = o);
    }

    private setTheme(urlPath: string) {
        const darkPaths = ['', '/'];
        this.theme = darkPaths.includes(urlPath) ? 'dark' : 'light';
    }

    public handleToggleSideNav(toggle: boolean) {
        this.sideNav.toggle();


    }


}
