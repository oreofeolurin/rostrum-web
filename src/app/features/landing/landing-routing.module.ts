import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LandingComponent} from './landing/landing.component';
import {IndexComponent} from './index/index.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {NoAuthGuard} from '../../core/guards/no-auth.guard';

const routes: Routes = [
    {
        path: '', component: LandingComponent,
        children: [
            {path: '', component: IndexComponent},
            {path: 'login', component: LoginComponent, canActivate: [NoAuthGuard]},
            {path: 'signup', component: SignupComponent, canActivate: [NoAuthGuard]}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LandingRoutingModule {
}
