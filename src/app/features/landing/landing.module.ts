import {NgModule} from '@angular/core';
import {LandingComponent} from './landing/landing.component';
import {LoginComponent} from './login/login.component';
import {LandingRoutingModule} from './landing-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {LoginService} from './login/login.service';
import { SignupComponent } from './signup/signup.component';
import {SignupService} from './signup/signup.service';
import {IndexComponent} from './index/index.component';

@NgModule({
    imports: [SharedModule,
        LandingRoutingModule
    ],
    declarations: [LandingComponent, IndexComponent, LoginComponent, SignupComponent],
    exports: [SharedModule],
    providers: [ LoginService, SignupService],
})
export class LandingModule {}

