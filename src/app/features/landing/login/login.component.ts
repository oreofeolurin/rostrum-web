import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {LoginService} from './login.service';
import {AuthService} from '../../../core/services/auth.service';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [
        trigger('stepState', [
            state('active', style({
                opacity: 1,
                transform: 'translateX(0%)'
            })),
            transition('void => *', [
                style({opacity: 0, transform: 'translateX(20%)'}),
                animate(500)
            ])
        ])]
})
export class LoginComponent implements OnInit {
    public loginForm: FormGroup;

    public error = null;
    public isGrinding = false;

    constructor(private formBuilder: FormBuilder,
                private loginService: LoginService,
                private authService: AuthService,
                private router: Router) {
    }

    ngOnInit() {

        this.loginForm = this.formBuilder.group({
            email: ['', Validators.email],
            password: ['', Validators.minLength(8)],
        });
    }


    public onSubmit(event) {
        if (this.loginForm.valid) {
            this.fireToAPI();
        } else {
            this.error = 'Please fill in valid values';
        }
    }

    private fireToAPI() {
        this.isGrinding = true;
        this.error = null;

        this.loginService.createAccessToken(this.loginForm.getRawValue()).subscribe(
            res => {
                if (this.authService.isLoggedIn) {
                    const redirectUrl = this.authService.redirectUrl || '/';
                    this.router.navigate([redirectUrl]);
                    this.isGrinding = false;
                }
            },
            err => {
                this.isGrinding = false;
                this.error = err.message;
            }
        );
    }

}
