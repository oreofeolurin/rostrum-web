import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {catchError, map} from 'rxjs/operators';

import {Service, RResponse, HttpStatus, AuthService} from '../../../core';

import {UserService} from '../../../store/user/user.service';

@Injectable()
export class LoginService extends Service {

    constructor(http: HttpClient, private authService: AuthService, private userService: UserService) {
        super(http);
    }

    /**
     * Create an access token
     *
     * @param {} credential
     * @return {Observable<any>}
     */
    public createAccessToken(credential) {

        return this.requestAccessToken(credential).pipe(
            map(response => {
                if (response.code === HttpStatus.OK) {
                    this.authService.isLoggedIn =
                        AuthService.saveAccessToken(response.body['token'])
                        && this.userService.saveUserData(response.body['user']);
                    return response;
                }
                throw response;
            }),
            catchError(this.handleError())
        );
    }


    /**
     * Prepares requestObject for creating session token and sends to server.
     *
     * @param {} credential - User Credential for login.
     * @return {Observable<RResponse>}
     */
    private requestAccessToken(credential) {

        return this.sendRequest('/account/create/user/token', credential);

    }

}
