import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {SignupService} from './signup.service';
import {RValidators} from '../../../core/helpers/rvalidators';
import {HttpStatus} from '../../../core/helpers/enums';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [
        trigger('stepState', [
            state('active', style({
                opacity: 1,
                transform: 'translateX(0%)'
            })),
            transition('void => *', [
                style({opacity: 0, transform: 'translateX(20%)'}),
                animate(500)
            ])
        ])]
})
export class SignupComponent implements OnInit {

    public signupForm: FormGroup;
    public error = null;
    public activeStep = 0;
    public isGrinding = false;

    constructor(private formBuilder: FormBuilder,
                private service: SignupService) {
    }

    ngOnInit() {

        this.signupForm = this.formBuilder.group({
            name: ['', Validators.required],
            email: ['', [RValidators.email], RValidators.userAvailabile(this.service, 'email')],
            password: ['', [Validators.minLength(8), Validators.required]],
            username: ['', [RValidators.username], RValidators.userAvailabile(this.service, 'username')],
        });
    }

    public nextStep(index: number, validate = true) {
        if (!validate || this.validateStep(index - 1)) {
            this.activeStep = index;
            this.error = null;
        } else {
            this.error = 'Please fill in valid values';
        }
    }

    public onSubmit(event) {
        if (this.signupForm.valid) {
            this.error = null;
            this.fireToAPI();

        } else {
            console.log(RValidators.getFormGroupErrors(this.signupForm));
            this.error = 'Please fill in valid values';
        }
    }

    public getStepState(step: number) {

        return this.activeStep === step ? 'active' : 'inactive';
    }

    private validateStep(step) {
        const {name, email, password} = this.signupForm.controls;
        switch (step) {
            case 0 :
                return email.valid;
            case 1 :
                return name.valid && password.valid;
        }

    }

    private fireToAPI() {
        this.isGrinding = true;

        const createAccountOptions = this.signupForm.getRawValue();
        this.service.createAccount(createAccountOptions).subscribe(
            res => {
                if (res.code === HttpStatus.CREATED) {
                    this.signupForm.reset();
                    this.nextStep(2, false);
                    this.isGrinding = false;
                }
            },
            err => {
                this.error = err.message;
                this.isGrinding = false;
            }
        );
    }

}
