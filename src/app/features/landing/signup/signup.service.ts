import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {catchError, map} from 'rxjs/operators';
import {RResponse, Service} from '../../../core/';
import {AuthService, HttpStatus} from '../../../core';

@Injectable()
export class SignupService extends Service {

    constructor(http: HttpClient) {
        super(http);
    }

    public checkUserAvailability(params) {
        return this.requestCheckAvailability(params).pipe(
            map(response => response.code === HttpStatus.OK),
            catchError(this.handleError())
        );
    }


    /**
     * Create an access token
     *
     * @param {Object} params
     * @return {Observable<RResponse>}
     */
    public createAccount(params: object) {
        return this.requestCreateAccount(params).pipe<RResponse>(
            catchError<RResponse, RResponse>(this.handleError())
        );
    }

    /**
     * Prepares requestObject for creating an account and sends to server.
     *
     * @param {object} createAccountParams - Create Account Params.
     * @return {Observable<RResponse>}
     */
    private requestCreateAccount(createAccountParams: object): Observable<RResponse> {

        return this.sendRequest('/account/create/user', createAccountParams);

    }


    /**
     * Prepares requestObject for checking user resource availability and sends to server.
     *
     * @param {object} availabilityParams - Check Avalability Params.
     * @return {Observable<RResponse>}
     */
    private requestCheckAvailability(availabilityParams: object): Observable<RResponse> {

        return this.sendRequest('/account/check/user/availability', null, 'GET', availabilityParams);

    }

}
