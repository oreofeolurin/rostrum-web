import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EditComponent} from './edit/edit.component';
import {EditResolver} from './edit/edit-resolver.service';
import {NewComponent} from './new/new.component';
import {PostComponent} from './post/post.component';

const routes: Routes = [
    {
        path: '',
        component: PostComponent,
        children: [
            {

                path: 'post/new',
                component: NewComponent
            },
            {
                path: 'post/:postId/edit',
                component: EditComponent,
                resolve: {
                    post: EditResolver
                }
            }
            ]
    }];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [EditResolver]
})
export class PostRoutingModule {
}
