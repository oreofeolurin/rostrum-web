import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PostRoutingModule} from './post-routing.module';
import {EditComponent} from './edit/edit.component';
import {EditorModule} from '../../shared/editor/editor.module';
import {NewComponent} from './new/new.component';
import {PostComponent} from './post/post.component';
import {SharedModule} from '../../shared/shared.module';
import {PublishComponent} from './publish/publish.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        PostRoutingModule,
        EditorModule
    ],
    declarations: [EditComponent, NewComponent, PostComponent, PublishComponent]
})
export class PostModule {
}
