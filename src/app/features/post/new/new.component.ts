import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PostContent} from '../../../store/post/post.interface';
import {PostEditorComponent} from '../../../shared/editor/post-editor/post-editor.component';
import {Post} from '../../../store';
import {UserService} from '../../../store/user/user.service';
import {Observable} from 'rxjs/Observable';
import {User} from '../../../store/user/user.interface';
import {Subscription} from 'rxjs/Subscription';
import {environment} from '../../../../environments/environment';
import {Modal, ModalExistRef, ModalInputs, ModalRef} from '../../../shared/common/modal/modal';
import {PublishComponent} from '../publish/publish.component';
import {ModalService} from '../../../core/services/modal.service';

@Component({
    selector: 'app-new',
    templateUrl: './new.component.html',
    styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit, OnDestroy {
    public STATIC_DOMAIN = environment.STATIC_DOMAIN;

    @ViewChild(PostEditorComponent)
    private editor: PostEditorComponent;


    public post: Post;
    public placeholder = 'Write something awesome';
    public editorEnabled = true;
    private user$: Subscription;
    public user: User;
    private modalRef: ModalRef;

    constructor(private router: Router,
                private modalService: ModalService,
                private userService: UserService) {
        this.user$ = userService.getUser().subscribe(user => this.user = user);
    }

    ngOnInit() {
    }

    public publish() {
        const content = this.editor.getEditorContent();

        if (content !== null) {

            this.post = Object.assign({}, this.post, {'content': content});
            const modalInput = new ModalInputs({token: PublishComponent.POST_INJECTION_TOKEN, value: this.post});
            const dialogModal = new Modal(PublishComponent, modalInput);

            this.modalRef = this.modalService.loadModal(dialogModal);

            const sub = this.modalRef.onExit().subscribe((ref: ModalExistRef) => {
                console.log(dialogModal.tag);
                if (ref.tag === dialogModal.tag && ref.resolve) {
                    this.router.navigateByUrl(ref.resolve);
                }
                sub.unsubscribe();
            });

        }

    }


    ngOnDestroy(): void {
        this.user$.unsubscribe();
    }


}
