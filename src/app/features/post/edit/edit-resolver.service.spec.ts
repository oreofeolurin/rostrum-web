import { TestBed, inject } from '@angular/core/testing';

import { EditResolver } from './edit-resolver.service';

describe('EditResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EditResolver]
    });
  });

  it('should be created', inject([EditResolver], (service: EditResolver) => {
    expect(service).toBeTruthy();
  }));
});
