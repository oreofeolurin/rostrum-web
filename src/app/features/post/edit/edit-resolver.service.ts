import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Post} from '../../../store';
import {catchError, map, switchMap, take} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {createAction} from '../../../store/create-action';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/app-state.interface';
import {UIActions} from '../../../store/ui/ui-actions';
import {UserService} from '../../../store/user/user.service';

@Injectable()
export class EditResolver implements Resolve<Post> {

    constructor(private service: UserService, private router: Router, private store: Store<AppState>) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const postId = route.paramMap.get('postId');

        this.store.dispatch(createAction(UIActions.LOAD));

        return this.service.getPostById(postId).pipe(
            map(res => res.body.posts[0]),
            map(post => {
                this.store.dispatch(createAction(UIActions.LOAD_DONE));
                if (post) {
                    return post;
                } else {
                    this.router.navigate(['/404']);
                    return null;
                }
            }),
            catchError(err => {
                this.store.dispatch(createAction(UIActions.LOAD_DONE));
                this.router.navigate(['/404']);
                return of(null);
            })
        );
    }
}
