import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Post} from '../../../store';
import {ActivatedRoute, Router} from '@angular/router';
import {PostContent} from '../../../store/post/post.interface';
import {PostEditorComponent} from '../../../shared/editor/post-editor/post-editor.component';
import {Subscription} from 'rxjs/Subscription';
import {isEqual} from 'lodash';
import {environment} from '../../../../environments/environment';
import {Modal, ModalExistRef, ModalInputs, ModalRef} from '../../../shared/common/modal/modal';
import {PublishComponent} from '../publish/publish.component';
import {ModalService} from '../../../core/services/modal.service';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit, OnDestroy {
    public STATIC_DOMAIN = environment.STATIC_DOMAIN;

    @ViewChild(PostEditorComponent)
    private editor: PostEditorComponent;


    public post: Post;
    public placeholder = 'Write something awesome';
    public editorEnabled = true;
    private SAVE_TIME_INTERVAL = 1000 * 20;
    private ticker$: Subscription;
    private modalRef: ModalRef;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private modalService: ModalService) {
        // this.ticker$ = interval(this.SAVE_TIME_INTERVAL).subscribe(() => this.save());
    }

    ngOnInit() {
        this.route.data
            .subscribe((data: { post: Post }) => {
                this.post = data.post;
                this.editor.content = this.post.content;
            });
    }

    public publish() {
        const content = this.editor.getEditorContent();

        if (content !== null) {

            this.post = Object.assign({}, this.post, {'content' : content});
            const modalInput = new ModalInputs({token: PublishComponent.POST_INJECTION_TOKEN, value: this.post});


            const dialogModal = new Modal(PublishComponent, modalInput);

            this.modalRef = this.modalService.loadModal(dialogModal);

            const sub = this.modalRef.onExit().subscribe((ref: ModalExistRef) => {
                console.log(ref);
                console.log(dialogModal.tag);
                if (ref.tag === dialogModal.tag && ref.resolve) {
                    this.router.navigateByUrl(ref.resolve);
                }
                sub.unsubscribe();
            });

        }

    }


    private save() {
        const content = this.editor.getEditorContent();
        console.log(isEqual(content, this.post.content));
       // console.log(content, this.post.content);
        if (!isEqual(content, this.post.content)) {
            console.log('saved');
            this.post = Object.assign(this.post, {content});
            // this.saveToDraft();
        } else {
            console.log('not saved');
        }
    }

    private saveToDraft() {
        console.log(this.post.content);
    }

    onUserSave(content: PostContent) {
        this.post = Object.assign(this.post, {content});
        this.saveToDraft();
    }


    ngOnDestroy () {
        // this.ticker$.unsubscribe();
    }


}

