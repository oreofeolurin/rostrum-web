import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatSidenav} from '@angular/material';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
    public sideNavOpened = false;
    public theme = 'light';

    @ViewChild('sideNav') sideNav: MatSidenav;

    constructor(private router: Router) {}

    public ngOnInit() {
        this.sideNav.openedChange.subscribe(o => this.sideNavOpened = o);
    }

    public handleToggleSideNav(toggle: boolean) {
        this.sideNav.toggle();
    }
}
