import {Component, InjectionToken, Injector, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpStatus, RValidators} from '../../../core';
import {ModalService} from '../../../core/services/modal.service';
import {Modal} from '../../../shared/common/modal/modal';
import {PostContent} from '../../../store/post/post.interface';
import {Observable} from 'rxjs/Observable';
import {Category, Post, PostService} from '../../../store';
import {CategoryService} from '../../../store/category/category.service';
import {Utils} from '../../../core/helpers/utils';
import {User} from '../../../store/user/user.interface';
import {UserService} from '../../../store/user/user.service';
import {Subscription} from 'rxjs/Subscription';
import {map, startWith} from 'rxjs/operators';
import {UIActions} from '../../../store/ui/ui-actions';

@Component({
    selector: 'app-publish',
    templateUrl: './publish.component.html',
    styleUrls: ['./publish.component.scss']
})
export class PublishComponent implements OnInit, OnDestroy {
    public static POST_INJECTION_TOKEN = new InjectionToken<Post>('post');

    private categories$: Subscription;
    private user$: Subscription;

    private categories: Array<Category>;


    public form: FormGroup;
    public tag: string;
    public meta: any;
    public user: User;
    public filteredCategories: Observable<Category[]>;
    public post: Post;
    private error: string = null;

    constructor(private formBuilder: FormBuilder,
                private modalService: ModalService,
                private postService: PostService,
                private uiActions: UIActions,
                categoryService: CategoryService,
                userService: UserService,
                injector: Injector) {

        this.tag = injector.get(Modal.TAG_INJECTION_TOKEN);
        this.post = injector.get(PublishComponent.POST_INJECTION_TOKEN);

        // Subscribe to category and user redux changes
        this.categories$ = categoryService.getCategories().subscribe(v => this.categories = v);
        this.user$ = userService.getUser().subscribe(v => this.user = v);
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            category: [this.post.category || '', Validators.required]
        });

        this.filteredCategories = this.form.get('category').valueChanges
            .pipe(
                startWith<string | Category>(''),
                map(value => typeof value === 'string' ? value : value.name),
                map(name => name ? this.filter(name) : this.categories.slice())
            );

        this.setTitleAndSummaryFromContent(this.post.content);


    }

    public onSubmit(event) {
        if (this.form.valid) {
            this.fireToAPI();
        }  else {
            this.error = 'Please fill in valid values';
        }
    }

    private fireToAPI() {
        this.uiActions.load();
        this.error = null;

        const post = Object.assign({}, {category: this.form.get('category').value._id}, this.meta, {content: this.post.content});
        let pubish$;

        if (this.post.postId) {
            pubish$ = this.postService.editPost(this.post.postId, post);
        } else {
            pubish$ = this.postService.createPost(post);
        }
        pubish$.subscribe(
            res => {
                this.uiActions.loadDone();
                if (res.code === HttpStatus.CREATED) {
                    this.callModalExit(`/${this.user.username}/${res.body.post.slug}`);
                }

                if (res.code === HttpStatus.OK && this.post.postId) {
                    this.callModalExit(`/${this.post.author.username}/${this.post.slug}`);
                }
            },
            err => {
                console.log(err);
                this.error = err.message;
                this.uiActions.loadDone();
            }
        );
    }


    public callModalExit(resolve: any, forceClose?: boolean) {
        this.modalService.notifyModalExit(this.tag, resolve, forceClose);
    }

    private setTitleAndSummaryFromContent(content: PostContent) {
        const raw = JSON.parse(this.post.content.raw);
        let metaText = [];

        for (let i = 0; i < raw.blocks.length; i++) {
            if (metaText.length >= 2) {
                break;
            }

            if (raw.blocks[i].text.trim().length > 0) {
                metaText.push(raw.blocks[i].text);
            }
        }

        // Lets get only the first sentence
        metaText = metaText.map(text => text.split('.')[0].trim());

        // Lets put full stop at the end of the summary
        metaText[1] = metaText[1] ? metaText[1] + '.' : null;

        // calculate readTime Value
        const readTime = Utils.calculateReadTime(Utils.renderHTMLToText(this.post.content.html));

        // lets set the meta values
        this.meta = {'title': metaText[0], 'summary': metaText[1], readTime};
    }


    public displayFn(category?: Category): string | undefined {
        return category ? category.name : undefined;
    }

    filter(name: string): Category[] {
        return this.categories.filter(c => c.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }

    ngOnDestroy(): void {
        this.categories$.unsubscribe();
        this.user$.unsubscribe();
    }
}
