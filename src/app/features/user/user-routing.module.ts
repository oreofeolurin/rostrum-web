import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PostResolver} from './post/post-resolver.service';
import {PostComponent} from './post/post.component';
import {UserComponent} from './user/user.component';

const routes: Routes = [
    {
        path: '',
        component: UserComponent,
        children : [
            {
                path: ':username/:slug',
                component: PostComponent,
                resolve: {
                    post: PostResolver
                }
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
    providers: [
        PostResolver
    ]
})
export class UserRoutingModule { }
