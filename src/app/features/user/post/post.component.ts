import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Post, PostService} from '../../../store';
import {environment} from '../../../../environments/environment';
import {UserService} from '../../../store/user/user.service';
import {Subscription} from 'rxjs/Subscription';
import {User} from '../../../store/user/user.interface';
import {UIActions} from '../../../store/ui/ui-actions';
import {PostComment} from '../../../store/post/post.interface';
import Animation from '../../../core/helpers/animations';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit, OnDestroy {

    @ViewChild('commentTextArea')
    commentTextArea: ElementRef;

    public post: Post;

    public STATIC_DOMAIN = environment.STATIC_DOMAIN;
    private user$: Subscription;
    public user: User;
    public postComments: Array<PostComment> = [];



    constructor(private route: ActivatedRoute,
                private postService: PostService,
                private uiActions: UIActions,
                userService: UserService) {
        this.user$ = userService.getUser().subscribe(v => this.user = v);
    }

    ngOnInit() {
        this.route.data
            .subscribe((data: { post: Post }) => {
                this.post = data.post;
                this.fetchPostResources(this.post);
            });
    }

    ngOnDestroy(): void {
        this.user$.unsubscribe();
    }

    triggerComment($event) {
        const text = this.commentTextArea.nativeElement.value;
        const commentParam = {content: {html: text, raw: null}};

        this.uiActions.load();

        this.postService.createCommentOnPost(this.post.postId, commentParam).subscribe(comment => {
            this.uiActions.loadDone();
            this.postComments.unshift(comment);
            Animation.scrollWindowTo('#comment-street', 20);
        });
    }

    private fetchPostResources(post: Post) {

        this.uiActions.load();
        this.postService.fetchCommentsOnPost(post.postId).subscribe(comments => {
            this.uiActions.loadDone();
            this.postComments = this.postComments.concat(comments);
        });

    }
}
