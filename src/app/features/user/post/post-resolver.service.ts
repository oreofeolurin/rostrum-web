import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Post} from '../../../store';
import {PostService} from '../../../store/post/post.service';
import {catchError, map, take} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {createAction} from '../../../store/create-action';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/app-state.interface';
import {UIActions} from '../../../store/ui/ui-actions';

@Injectable()
export class PostResolver implements Resolve<Post> {
    constructor(private service: PostService, private router: Router, private store: Store<AppState>) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const slug = route.paramMap.get('slug');
        this.store.dispatch(createAction(UIActions.LOAD));

        return this.service.fetchPosts({slug}).pipe(
            /// take(1),
            map(res => res.body.posts[0]),
            map(post => {
                this.store.dispatch(createAction(UIActions.LOAD_DONE));
                if (post) {
                    return post;
                } else {
                    this.router.navigate(['/']);
                    return null;
                }
            }),
            catchError(err => {
                this.router.navigate(['/']);
                return of(null);
            })
        );
    }
}
