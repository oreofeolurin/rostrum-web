import {Component, OnInit, ViewChild} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {MatSidenav} from '@angular/material';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserComponent implements OnInit {
    public sideNavOpened = false;
    public theme = 'light';

    @ViewChild('sideNav') sideNav: MatSidenav;

    constructor(private router: Router) {}

    public ngOnInit() {
        this.sideNav.openedChange.subscribe(o => this.sideNavOpened = o);
    }

    public handleToggleSideNav(toggle: boolean) {
        this.sideNav.toggle();
    }


}

