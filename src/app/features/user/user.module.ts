import { NgModule } from '@angular/core';

import { UserRoutingModule } from './user-routing.module';
import { PostComponent } from './post/post.component';
import {SharedModule} from '../../shared/shared.module';
import { UserComponent } from './user/user.component';
import {EditorModule} from '../../shared/editor/editor.module';

@NgModule({
  imports: [
      SharedModule,
      EditorModule,
    UserRoutingModule
  ],
  declarations: [PostComponent, UserComponent]
})
export class UserModule { }
