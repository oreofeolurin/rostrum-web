import {Exception} from '../../core/exceptions/custom.exception';
import {User} from '../user/user.interface';

export interface PostList {
    posts: Array<Post>;
    loading: boolean;
    error: Exception;
}

export interface Post {
    readonly postId: string;
    readonly title: string;
    readonly slug: string;
    readonly url: string;
    readonly author: {username: string, name: string, summary: string};
    readonly category: {name: string};
    readonly summary: string;
    readonly body: string;
    readonly content: PostContent;
    readonly createdAt: string;
    readonly readTime: number;
    comments: Array<PostComment>;
}

export interface PostComment {
    readonly _id: any;
    readonly content: PostContent;
    readonly user: User;
    readonly createdAt: string;
}

export interface PostContent {
    readonly html: string;
    readonly raw: string;
}
