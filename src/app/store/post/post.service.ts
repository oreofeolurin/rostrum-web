import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {AppState} from '../app-state.interface';
import {Post, PostComment} from './post.interface';
import {catchError, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {RResponse, Service} from '../../core';

@Injectable()
export class PostService extends Service {

    constructor(http: HttpClient, private store: Store<AppState>) {
        super(http);
    }



    /**
     * Sends edit post request to server.
     *
     * @return {Observable<RResponse>}
     */
    editPost(postId, body) {
        return this.sendRequest(`/post/${postId}`, body, 'PUT').pipe(catchError(this.handleError('[Post] Edit'))) as Observable<RResponse>;
    }

    /**
     * Sends create post request to server.
     *
     * @return {Observable<RResponse>}
     */
    createPost(body) {
        return this.sendRequest('/post', body).pipe(catchError(this.handleError('[Post] Create'))) as Observable<RResponse>;
    }

    /**
     * Sends fetch post request to server.
     *
     * @return {Observable<RResponse>}
     */
    fetchPosts(params: object) {
        return this.sendRequest('/post', null, 'GET', params)
            .pipe(catchError(this.handleError('[Post] Fetch'))) as Observable<RResponse>;
    }

    /**
     * Sends fetch latest post request to server.
     *
     * @return {Observable<RResponse>}
     */
    fetchLatestPosts() {
        return this.sendRequest('/post')
            .pipe( map(res => res.body.posts), catchError(this.handleError('[Latest Post] Fetch'))) as Observable<Array<Post>>;
    }


    /**
     * Sends fetch latest post request to server.
     *
     * @return {Observable<RResponse>}
     */
    createCommentOnPost(postId, body) {
        return this.sendRequest(`/post/${postId}/comment`, body)
            .pipe(
                map(res => res.body.comment),
                catchError(this.handleError('[Comment] Create')
                )
            ) as Observable<PostComment>;
    }

    /**
     * Sends fetch latest post request to server.
     *
     * @return {Observable<RResponse>}
     */
    fetchCommentsOnPost(postId) {
        return this.sendRequest(`/post/${postId}/comment`, null, 'GET')
            .pipe(
                map(res => res.body.comments),
                catchError(this.handleError('[Comment] Fetch')
                )
            ) as Observable<PostComment>;
    }

}
