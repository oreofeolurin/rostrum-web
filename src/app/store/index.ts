import {userStateReducer} from './user/user-reducer';
export {PostService} from './post/post.service';
export {Post} from './post/post.interface';

export {CategoryActions} from './category/category-actions';
export {CategoryService} from './category/category.service';
export {Category} from './category/category.interface';

import {categoryReducer} from './category/category-reducer';


import {uiReducer} from './ui/ui-reducer';

export const rootReducer = {
    ui: uiReducer,
    categoryList: categoryReducer,
    userState: userStateReducer,
};


export const rootEffects = [];

