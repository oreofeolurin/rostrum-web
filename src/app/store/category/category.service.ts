import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {AppState} from '../app-state.interface';
import {catchError, map} from 'rxjs/operators';
import {Service, RResponse} from '../../core';
import {HttpClient} from '@angular/common/http';
import {Category} from './category.interface';
import {CategoryActions} from './category-actions';
import {of} from 'rxjs/observable/of';

@Injectable()
export class CategoryService extends Service {

    constructor(http: HttpClient,
                private store: Store<AppState>,
                private actions: CategoryActions) {
        super(http);
        this.initializeCategories();
    }

    public initializeCategories() {
        console.log('About to initialize categories');
        this.fetchCategories().subscribe(
            res => this.actions.loadSuccess(res.body.categories),
            err => this.actions.loadError(err)
        );
    }

    getCategories(): Observable<Array<Category>> {
        return this.store.select(state => {
            return state.categoryList.categories;
        });
    }

    /**
     * Prepares requestObject for fetching post and sends to server.
     *
     * @return {Observable<RResponse>}
     */
    fetchCategories() {
        return this.sendRequest('/category').pipe(catchError(this.handleError('[Category] Fetch'))) as Observable<RResponse>;
    }

}
