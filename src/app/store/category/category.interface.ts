import {Exception} from '../../core';


export interface CategoryList {
    categories: Array<Category>;
    loading: boolean;
    error: Exception;
}

export interface Category {
    readonly id: string;
    readonly name: string;
}
