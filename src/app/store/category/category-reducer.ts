import {CategoryActions} from './category-actions';
import {PayloadAction} from '../create-action';
import {CategoryList} from './category.interface';

const INITIAL_STATE: CategoryList = {categories: [], loading: false, error: null};

export function categoryReducer(state: CategoryList = INITIAL_STATE, action: PayloadAction): CategoryList {
    switch (action.type) {
        case CategoryActions.LOAD:
            return {...state, loading: true};
        case CategoryActions.LOAD_ABORTED:
            return {...state, loading: false};
        case CategoryActions.LOAD_SUCCESS:
            return {...state, categories: action.payload, loading: false, error: null};
        case CategoryActions.LOAD_ERROR:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
}
