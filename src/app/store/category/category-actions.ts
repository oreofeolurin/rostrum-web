import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {createAction} from '../create-action';
import {AppState} from '../app-state.interface';
import {Category} from './category.interface';

@Injectable()
export class CategoryActions {

    static LOAD = '[Category] Load';
    static LOAD_ABORTED = '[Category] Aborted';
    static LOAD_SUCCESS = '[Category] Success';
    static LOAD_ERROR = '[Category] Error';
    static RESET = '[Category] Reset';

    constructor(private store: Store<AppState>) {

    }

    load() {
        this.store.dispatch(createAction(CategoryActions.LOAD));
    }

    loadSuccess(categories: Array<Category>) {
        this.store.dispatch(createAction(CategoryActions.LOAD_SUCCESS, categories));
    }

    loadError(err) {
        this.store.dispatch(createAction(CategoryActions.LOAD_ERROR, err));
    }

    reset() {
        this.store.dispatch(createAction(CategoryActions.RESET));
    }

}
