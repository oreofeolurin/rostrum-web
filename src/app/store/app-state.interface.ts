import {CategoryList} from './category/category.interface';
import {UIState} from './ui/ui-state.interface';
import {UserState} from './user/user.interface';

export interface AppState {
    readonly ui: UIState;
    readonly categoryList: CategoryList;
    readonly userState: UserState;
}

