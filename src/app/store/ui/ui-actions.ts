import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {createAction} from '../create-action';
import {AppState} from '../app-state.interface';

@Injectable()
export class UIActions {

    static LOAD = '[UI State] Load';
    static LOAD_DONE = '[UI State] Load Done';
    static LOAD_ERROR = '[UI State] Load Error';

    constructor(private store: Store<AppState>) {
    }

    load() {
        this.store.dispatch(createAction(UIActions.LOAD));
    }

    loadDone() {
        this.store.dispatch(createAction(UIActions.LOAD_DONE));
    }

}
