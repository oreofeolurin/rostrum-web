import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {AppState} from '../app-state.interface';


@Injectable()
export class UIService {

    constructor(private store: Store<AppState>) {
    }


    getIsLoading(): Observable<boolean> {
        return this.store.select(state => state.ui.loading);
    }

}
