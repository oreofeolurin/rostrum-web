import {PayloadAction} from '../create-action';
import {UIState} from './ui-state.interface';
import {UIActions} from './ui-actions';

const INITIAL_STATE: UIState = {loading: false, error: null};

export function uiReducer(state: UIState = INITIAL_STATE, action: PayloadAction): UIState {
    switch (action.type) {
        case UIActions.LOAD:
            return {...state, loading: true};
        case UIActions.LOAD_DONE:
            return {...state, loading: false};
        case UIActions.LOAD_ERROR:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
}
