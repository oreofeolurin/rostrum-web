import {Action} from '@ngrx/store';

export type PayloadAction = Action & {payload: any};

export function createAction(type, payload?): PayloadAction {
    return { type, payload };
}
