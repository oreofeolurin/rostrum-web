import {NgModule} from '@angular/core';
import {EffectsModule} from '@ngrx/effects';
import {rootReducer} from './index';
import {StoreModule} from '@ngrx/store';
import {environment} from '../../environments/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {PostService} from './post/post.service';
import {CategoryActions} from './category/category-actions';
import {CategoryService} from './category/category.service';
import {UIActions} from './ui/ui-actions';
import {UIService} from './ui/ui.service';
import {rootEffects} from './index';
import {UserService} from './user/user.service';
import {UserActions} from './user/user-actions';

@NgModule({
    imports: [
        StoreModule.forRoot(rootReducer),
        // Instrumentation must be imported after importing StoreModule (config is optional)
        StoreDevtoolsModule.instrument({
            maxAge: 25, // Retains last 25 states
            logOnly: environment.production // Restrict extension to log-only mode
        }),
        EffectsModule.forRoot(rootEffects),
    ],
    providers: [
        PostService,
        CategoryService, CategoryActions,
        UIActions, UIService,
        UserService, UserActions
    ]
})
export class RxStoreModule {}
