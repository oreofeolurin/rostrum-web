import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {createAction} from '../create-action';
import {AppState} from '../app-state.interface';
import {User} from './user.interface';

@Injectable()
export class UserActions {

    static LOAD = '[User] Load';
    static LOAD_SUCCESS = '[User] Load Success';
    static LOAD_ERROR = '[User] Load Error';

    constructor(private store: Store<AppState>) {
    }

    load() {
        this.store.dispatch(createAction(UserActions.LOAD));
    }

    loadSuccess(user: User) {
        this.store.dispatch(createAction(UserActions.LOAD_SUCCESS, user));
    }

    loadError() {
        this.store.dispatch(createAction(UserActions.LOAD_ERROR));
    }

}
