import {PayloadAction} from '../create-action';
import {UserState} from './user.interface';
import {UserActions} from './user-actions';


const INITIAL_STATE: UserState = {loading: false, error: null, user: null};

export function userStateReducer(state: UserState = INITIAL_STATE, action: PayloadAction): UserState {
    switch (action.type) {
        case UserActions.LOAD:
            return {...state, loading: true};
        case UserActions.LOAD_SUCCESS:
            return {...state, user: action.payload, loading: false, error: null};
        case UserActions.LOAD_ERROR:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
}
