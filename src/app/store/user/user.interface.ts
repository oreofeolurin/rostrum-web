export interface UserState {
    loading: boolean;
    error: null;
    user: User;
}

export interface User {
    name: string;
    username: string;
    email: string;
    summary: string;
}
