import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RResponse, Service} from '../../core';
import {LocalStoreKey} from '../../core/helpers/enums';
import {LocalStore} from '../../core/helpers/local-store';
import {UserActions} from './user-actions';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {AppState} from '../app-state.interface';
import {User} from './user.interface';
import {catchError} from 'rxjs/operators';


@Injectable()
export class UserService extends Service {

    constructor(http: HttpClient, private actions: UserActions, private store: Store<AppState>) {
        super(http);
        this.initializeUser();
    }

    public initializeUser() {
        console.log('user loded initilition');
        const user = LocalStore.getFromStore(LocalStoreKey.USER_DATA);
        user && this.actions.loadSuccess(user);
    }

    public saveUserData(data): boolean {
        const saved =  LocalStore.saveToStore(LocalStoreKey.USER_DATA, data);
        saved && this.actions.loadSuccess(data);

        console.log('user sved fter login', data);
        return saved;
    }

    public removeData() {
        this.actions.loadSuccess(null);
    }

    getUser(): Observable<User> {
        return this.store.select(state => state.userState.user);
    }


    /**
     * Sends fetch post request to server.
     *
     * @return {Observable<RResponse>}
     */
    getPostById(postId: string) {
        return this.sendRequest(`/me/post/${postId}`, null, 'GET')
            .pipe(catchError(this.handleError('[User] Fetch Post By Id'))) as Observable<RResponse>;
    }
}
