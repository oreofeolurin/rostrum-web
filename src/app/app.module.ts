import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {AppComponent} from './features/app/app.component';
import {LandingModule} from './features/landing/landing.module';
import {NotFoundComponent} from './features/not-found/not-found.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {CoreModule} from './core/core.module';
import {RxStoreModule} from './store/rx-store.module';
import {UserModule} from './features/user/user.module';
import {PostModule} from './features/post/post.module';


@NgModule({
    declarations: [
        AppComponent,
        NotFoundComponent
    ],
    imports: [
        BrowserModule.withServerTransition({appId: 'rostrum-web'}),
        BrowserAnimationsModule,
        HttpClientModule,
        CoreModule,
        RxStoreModule,
        LandingModule,
        PostModule,
        UserModule,
        RouterModule.forRoot([
            {path: '404', component: NotFoundComponent},
            {path: '**', component: NotFoundComponent},
        ])
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
