import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Subject} from 'rxjs/Subject';
import {Modal, ModalExistRef, ModalRef} from '../../shared/common/modal/modal';

@Injectable()
export class ModalService {

    // Observable string sources
    private modalExitSubject = new Subject<ModalExistRef>();
    private newModalSubject = new BehaviorSubject<Modal>(null);

    // Observable string streams
    public modalExitStream = this.modalExitSubject.asObservable();
    public newModalStream = this.newModalSubject.asObservable();

    constructor() {
    }

    public notifyModalExit(tag: string, resolve: any, forceClose?: boolean) {
        if (typeof forceClose === 'undefined') { forceClose = false; }
        this.modalExitSubject.next({tag: tag, resolve: resolve, forceClose: forceClose});
    }

    public loadModal(modal: Modal) {
        this.newModalSubject.next(modal);
        return new ModalRef(this.modalExitStream);
    }

}
