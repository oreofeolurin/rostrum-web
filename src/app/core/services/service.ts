import {environment} from '../../../environments/environment';
import {AppException} from '../exceptions/app.exception';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {RResponse} from '../helpers/rresponse';
import {Observable} from 'rxjs/Observable';
import {Exception} from '../exceptions/custom.exception';
import {AppStatus} from '../helpers/enums';

interface RequestOpts {
    method: string;
    url: string;
    options: object;
}

export class Service {

    /**
     * Service constructor.
     *
     * @constructor
     * @param http.
     * @param apiBase
     */
    public constructor(protected http: HttpClient, private apiBase = environment.API_BASE) {
    }


    // noinspection JSMethodCanBeStatic
    protected getCombinedHeaders(headers: HttpHeaders) {
        headers.set('Content-Type', 'application/json');
        return headers;
    }

    /**
     * Prepares request object to send to server.
     *
     * @param {string} url - Request url.
     * @param {Object} body - Request Body.
     * @param {RequestMethod} method - Request Method.
     * @param params
     * @param {Headers} headers - Request Headers.
     * @return {Request}
     */
    public prepareRequest(url: string, body: Object = null,
                          method: string = 'POST',
                          params = null,
                          headers: HttpHeaders = new HttpHeaders()): RequestOpts {

        const combinedHeaders = this.getCombinedHeaders(headers);

        if (params !== null) {
            params = new HttpParams({fromObject: params});
        }

        return {method, url: this.apiBase + url, options: {body, headers: combinedHeaders, params: params}};
    }


    /**
     * Sends request object to server and handles apropiate callbacks.
     *
     * @param {RequestOpts} requestOpts Request object
     * @return {Observable<RResponse>}
     */
    public sendToServer(requestOpts: RequestOpts): Observable<RResponse> {
        return this.http.request<RResponse>(requestOpts.method, requestOpts.url, requestOpts.options);
    }


    /**
     * Builds the RequestOpts and sends to server immediately
     *
     * @param {string} url
     * @param {Object} body
     * @param {string} method
     * @param {any} paramsObject
     * @param {HttpHeaders} headers
     * @return {Observable<RResponse>}
     */
    public sendRequest(url: string, body: Object = null,
                       method: string = body == null ? 'GET' : 'POST',
                       paramsObject = null,
                       headers: HttpHeaders = new HttpHeaders()) {

        const requestOpts = this.prepareRequest(url, body, method, paramsObject, headers);

        return this.sendToServer(requestOpts);
    }


    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * TODO: send the error to remote logging infrastructure
     *
     * @param operation - name of the operation that failed
     *
     */
    public handleError(operation = 'operation') {
        return (err: any) => {
            const error = this.catchErrors(err);

            // display error for user consumption
            // TODO: better job of transforming error for user consumption
            console.log(`${operation} failed: ${error.getMessage()}`);

            throw error;
        };
    }


    // noinspection JSMethodCanBeStatic
    /**
     * Utility function to catch errors
     * basically it checks if its an unauthorised error
     * it logs out immediately if its an unauthorised error
     *
     * @param err
     */
    private  catchErrors(err: HttpErrorResponse): Exception {
        let error;

        console.log(err);

        if (err.error instanceof Error) {
            // A client-side or network error occurred. Handle it accordingly.
            error = new AppException(err.error);

        } else if (err.status === 0) {
            error = AppException.INTERNET_UNAVAILABLE;
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            error = new AppException(err.error.error, err.error.code, err.error.message);
        }

        console.log(`Backend returned code ${err.status}, body was: ${JSON.stringify(err.error)}`);

        return error;
    }

    // TODO: better job of transforming error for user consumption
    private displayUserFriendlyError(operation, error) {
        console.log(`${operation} failed: ${error.getMessage()}`);

        if (error.code === AppStatus.INTERNET_UNAVAILABLE) {
            // alert('alert no internet');
        }
    }
}
