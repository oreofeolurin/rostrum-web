import {Injectable} from '@angular/core';
import {Service} from './service';
import {LocalStore} from '../helpers/local-store';
import {JwtHelper} from '../helpers/jwt-helper';
import {HttpClient} from '@angular/common/http';
import {LocalStoreKey} from '../helpers/enums';

@Injectable()
export class AuthService extends Service {

    public isLoggedIn = false;

    // store the URL so we can redirect after logging in
    public redirectUrl: string;


    // private modalRef: ModalRef;

    /**
     * AuthService constructor.
     *
     * @constructor
     * @param {HttpClient} http.
     */
    public constructor(http: HttpClient) {
        super(http);
        this.authenticateAccessToken();
    }


    public static saveAccessToken(token): boolean {
        return LocalStore.saveToStore(LocalStoreKey.JWT_TOKEN, token);
    }

    public static getAccessToken() {
        return LocalStore.getFromStore(LocalStoreKey.JWT_TOKEN);
    }


    public static getAuthorizationHeader() {
        return this.getAccessToken() && {Authorization: `RJT ${this.getAccessToken()}` };
    }

    public logout() {
        if (LocalStore.deleteStore(LocalStoreKey.STORE_KEY)) {
            this.isLoggedIn = false;
        }
    }


    private authenticateAccessToken() {
        const token = AuthService.getAccessToken();

        if (JwtHelper.tokenNotExpired(token)) {
            this.isLoggedIn = true;
        }
    }

    public getDecodedAccessToken() {

        const token = LocalStore.getFromStore(LocalStoreKey.JWT_TOKEN);

        return JwtHelper.decodeToken(token);
    }


}
