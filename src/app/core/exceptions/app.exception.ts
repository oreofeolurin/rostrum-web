import {AppStatus, HttpStatus} from '../helpers/enums';
import {Exception} from './custom.exception';

export class AppException extends Exception {

    constructor(err: any, code: number = HttpStatus.INTERNAL_SERVER_ERROR, message?: string) {
        super(err, code, message);
    }

    public static get INTERNET_UNAVAILABLE() {
        return new this('Internet not available', AppStatus.INTERNET_UNAVAILABLE);
    }
}
