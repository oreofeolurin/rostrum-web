import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable()
export class MockServerInterceptor implements HttpInterceptor {

    constructor() {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (req.url.includes(environment.API_BASE)) {

            return next.handle(alterRequest(req)).pipe(map(event => alterResponse(req, event)));
        }

        return next.handle(req);

    }
}

function alterRequest(req: HttpRequest<any>) {

    let authReq = req;

    switch (req.url.substr(environment.API_BASE.length)) {
        case '/posts' :
        case '/categories' :
            authReq = req.clone({url: environment.MOCK_API_BASE + req.url.substr(environment.API_BASE.length) });
    }

    return authReq;
}


function alterResponse(req, event: HttpEvent<any>) {

    if (event instanceof HttpResponse && (event.status / 100) < 3) {
        switch (req.url.substr(environment.API_BASE.length)) {
            case '/posts' :
                return event.clone({body: createResponse({posts: event.body})});
            case '/categories' :
                return event.clone({body: createResponse({categories: event.body})});
            default:
                return event;
        }
    }

    return event;
}


function createResponse(body, code = 200, message = 'Request Successful') {
    return {code, message, body};
}
