import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {AuthService} from '../services/auth.service';
import {environment} from '../../../environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    private authHeaders;
    constructor() { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Get the auth header from the service.
        this.authHeaders = this.authHeaders || AuthService.getAuthorizationHeader();

        // Clone the request to add the new header only if URL is for API_BASE.
        const authReq = this.authHeaders && req.url.includes(environment.API_BASE) ? req.clone({setHeaders: this.authHeaders}) : req;

        // Pass on the cloned request instead of the original request.
        return next.handle(authReq);
    }
}
