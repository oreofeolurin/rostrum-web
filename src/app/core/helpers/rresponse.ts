import {Category, Post} from '../../store';
import {PostComment} from '../../store/post/post.interface';

export class RResponse {
    public readonly code: number;
    public readonly message: string;
    public readonly body: RBody;
}

export class RBody {
    post: Post;
    posts: Array<Post>;
    category: Category;
    categories: Array<Category>;
    comment: PostComment;
    comments: Array<PostComment>;
}
