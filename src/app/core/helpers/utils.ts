import {words} from 'lodash';

export class Utils {

    public static generateRandomID(length): string {

        const id = Math.random().toString(36).substr(2, length);

        if (id.length === length) {
            return id;
        }

        return Utils.generateRandomID(length);
    }


    public static calculateReadTime(value: any, args?: any) {
        return Math.ceil((words(value).length) / 200);
    }

    public static renderHTMLToText(html: string) {

        const tmp = document.createElement('DIV');
        tmp.innerHTML = html;
        return tmp.textContent.trim() || tmp.innerText.trim() || '';
    }


}
