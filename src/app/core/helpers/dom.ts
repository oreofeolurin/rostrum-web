import {ElementRef} from '@angular/core';

export class DOM {
    private rootElement: any;

    constructor(private el: any) {
        if (el instanceof ElementRef)
            this.rootElement = this.el.nativeElement;

        else this.rootElement = el;
    }

    public select(selectorStr: string): DOMElements;
    public select(element: HTMLElement): DOMElements;
    public select(item: any): DOMElements {

        if (typeof item === 'string')
            return this.selectElementWithString(item);


        if (item instanceof HTMLElement)
            return this.selectElementWithElement(item);
    }


    public selectOne(item: any): DOMElements {
        let domElements = this.select(item);
        return new DOMElements(domElements.elements[0]);
    }


    private selectElementWithElement(element: HTMLElement): DOMElements {
        return new DOMElements(element);
    }


    private selectElementWithString(selectorStr: string) {
        let domElements = [].slice.call(this.rootElement.querySelectorAll(selectorStr));
        return new DOMElements(domElements);
    }

}

export class DOMElements {

    public elements: Array<Element>;
    public element: HTMLElement | Element;


    constructor(item: Element | Array<Element>) {

        if (item instanceof HTMLElement || item instanceof Element) {
            this.element = item;
        } else if (item instanceof Array) {
            this.elements = item;
        }


    }

    public bind(eventName: string, func: any) {

        if (typeof this.elements !== 'undefined') {
            for (const element of this.elements) {
                DOMEvents.registerEvent(element, eventName, func);
            }
        }

        if (typeof this.element !== 'undefined') {
            DOMEvents.registerEvent(this.element, eventName, func);
        }
    }

    public once(eventName: string, func: any) {

        if (typeof this.elements !== 'undefined') {
            for (const element of this.elements) {
                DOMEvents.registerEvent(element, eventName, () => {
                    func(); DOMEvents.unregisterEvents(element, eventName)});
            }
        }

        if (typeof this.element !== 'undefined') {
            DOMEvents.registerEvent(this.element,eventName, ()=> {
                func(); DOMEvents.unregisterEvents(this.element,eventName)});
        }
    }

    public unbind(eventName: string) {


        if (typeof this.elements !== 'undefined') {
            for (let element of this.elements) {
                DOMEvents.unregisterEvents(element,eventName);
            }
        }

        if (typeof this.element !== 'undefined') {
            DOMEvents.unregisterEvents(this.element,eventName);
        }


    }


    public remove() {


        if (typeof this.elements !== 'undefined') {
            for (let element of this.elements) {
                element.parentElement.removeChild(element);
            }


        }

        if (typeof this.element !== 'undefined') {

            this.element.parentElement.removeChild(this.element);

        }

    }

    public prepend(html) {

        if (typeof this.elements !== 'undefined') {
            for (let element of this.elements)
                element.innerHTML = html + element.innerHTML;
        }

        if (typeof this.element !== 'undefined') {
            this.element.innerHTML = html + this.element.innerHTML;
        }


    }

    public append(html) {

        if (typeof this.elements !== 'undefined') {
            for (let element of this.elements)
                element.innerHTML = element.innerHTML + html;
        }

        if (typeof this.element !== 'undefined') {
            this.element.innerHTML = this.element.innerHTML + html;
        }



    }

    public html(html?: string): any {
        if (typeof(html) === 'undefined') {

            if (typeof this.elements !== 'undefined')
                return this.elements[0].innerHTML;

            if (typeof this.element !== 'undefined')
                return this.element.innerHTML;

        }
        else {

            if (typeof this.elements !== 'undefined') {
                for (let element of this.elements)
                    element.innerHTML = html;
                return this;
            }

            if (typeof this.element !== 'undefined') {
                this.element.innerHTML = html;
                return this
            }

        }
    }

    public empty() {

        if (typeof this.elements !== 'undefined') {
            for (let element of this.elements)
                element.innerHTML = '';
        }

        if (typeof this.element !== 'undefined')
            this.element.innerHTML = '';

    }


    public val(value?: string) {

        if (typeof this.element !== 'undefined'){

            if(typeof value !== 'undefined')
                this.element['value'] = value;
            else
                return this.element['value'];
        }
    }

    public removeAttr(name) {

        if (typeof this.element !== 'undefined') {
            this.element.removeAttribute(name);
            return this;
        }

    }

    public attr(name, value) {

        if (typeof this.element !== 'undefined') {
            this.element.setAttribute(name, value);
            return this;
        }

    }

    public addClass(name): DOMElements {

        function addClassNameToString(element, name) {

            if (element.className.length == 0)
                element.className = name;

            else {
                let classArray = element.className.split(' ');
                if (!classArray.includes(name)) {
                    classArray.push(name);
                    element.className = classArray.join(' ');

                }
            }

        }

        if (typeof this.elements !== 'undefined') {
            for (let element of this.elements)
                addClassNameToString(element, name);

        }

        if (typeof this.element !== 'undefined')
            addClassNameToString(this.element, name);


        return this;

    }

    public removeClass(name) {

        if (typeof this.elements !== 'undefined') {
            for (let element of this.elements)
                removeClassNameToString(element, name);
        }

        if (typeof this.element !== 'undefined')
            removeClassNameToString(this.element, name);


        function removeClassNameToString(element, name) {
            let classArray = element.className.split(' ');
            if (classArray.includes(name)) {
                classArray = classArray.filter((v) => v != name);
                element.className = classArray.join(' ');
            }

        }

    }


}


//https://stackoverflow.com/a/2837906/3335054
export class DOMEvents{

    public static registerEvent(el, eventName, funct) {

        if (el.attachEvent) {
            el['e'+eventName+funct] = funct;
            el[eventName+funct] = function(){el['e'+eventName+funct](window.event);}
            el.attachEvent( 'on'+eventName, el[eventName+funct] );
        } else {
            el.addEventListener(eventName, funct, false);
        }

        if(!el.eventHolder) el.eventHolder = [];
        el.eventHolder[el.eventHolder.length] = new Array(eventName, funct);

    }


    public  static unregisterEvent(obj, type, fn){
        if (obj.detachEvent) {
            obj.detachEvent( 'on'+type, obj[type+fn] );
            obj[type+fn] = null;
        } else {
            obj.removeEventListener( type, fn, false );
        }
    }

    public static hasEvent(el, eventName, funct) {

        if (!el.eventHolder) {
            return false;
        } else {
            for (let i = 0; i < el.eventHolder.length; i++) {
                if (el.eventHolder[i][0] == eventName && String(el.eventHolder[i][1]) == String(funct)) {
                    return true;
                }
            }
        }
        return false;
    }


    public static unregisterEvents(el, eventName) {

        if (el.eventHolder) {

            let removed = 0;
            for (let i = 0; i < el.eventHolder.length; i++) {
                if (el.eventHolder[i][0] == eventName) {
                    this.unregisterEvent(el, eventName, el.eventHolder[i][1]);
                    el.eventHolder.splice(i, 1);
                    removed++;
                    i--;
                }
            }

            return (removed > 0);
        } else {
            return false;
        }
    }
}
