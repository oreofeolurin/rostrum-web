import {FormControl, ValidatorFn, ValidationErrors, AbstractControl, FormGroup} from '@angular/forms';
import {Validator} from 'class-validator';
import {map} from 'rxjs/operators/map';
import {SignupService} from '../../features/landing/signup/signup.service';

const validator = new Validator();

export class RValidators {

    public static email(c: FormControl) {
        return validator.isEmail(c.value) ? null : {email: {valid: false}};
    }

    public static alpha(c: FormControl) {
        return validator.isAlpha(c.value) ? null : {name: {valid: false}};
    }

    public static username(c: FormControl) {
        return validator.matches(c.value, /^(?![0-9])\w{3,}/)
        && validator.isAlphanumeric(c.value) ? null : {username: {valid: false}};
    }

    public static userAvailabile(service: SignupService, key: string) {
        return (control: AbstractControl) => {
            return service.checkUserAvailability({[key]: control.value}).pipe(
                map(res => res ? null : {userNotAvailable: true})
            );
        };
    }


    public static getFormGroupErrors(form: FormGroup) {
        const formErrors = [];

        Object.keys(form.controls).forEach(key => {
            const errors = form.get(key).errors;
            if (errors !== null) {
                formErrors[key] = Object.keys(form.get(key).errors);
            }
        });


        return formErrors;
    }

}
